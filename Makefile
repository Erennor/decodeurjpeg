
#Repertoires du projet

OBJ_DIR = bin
SRC_DIR = src
INC_DIR = include

# Options de compilation 

CC = gcc
LD = gcc
INC = -I$(INC_DIR)
CFLAGS = $(INC) -Wall -std=c99 -O0 -Wextra -g #-pg #-O3
LDFLAGS = -lm #-pg


# Règle générique

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(INC_DIR)/%.h
	$(CC) -c $(CFLAGS) $< -o $@

# Liste des objets encadrants

OBJ_FILES = $(OBJ_DIR)/main.o $(OBJ_DIR)/idct.o  $(OBJ_DIR)/conv.o $(OBJ_DIR)/iqzz.o 
OBJ_FILES += $(OBJ_DIR)/upsampler.o $(OBJ_DIR)/huffman.o $(OBJ_DIR)/unpack.o $(OBJ_DIR)/bitstream.o
OBJ_FILES += $(OBJ_DIR)/tiff.o

# Liste des objets realises

NEW_OBJ_FILES = $(OBJ_DIR)/idct.o $(OBJ_DIR)/iqzz.o $(OBJ_DIR)/conv.o $(OBJ_DIR)/unpack.o $(OBJ_DIR)/upsampler.o $(OBJ_DIR)/huffman.o $(OBJ_DIR)/bitstream.o $(OBJ_DIR)/main.o $(OBJ_DIR)/tiff.o 



all : jpeg2tiff
test : jpeg2tiff
	rm -f tests/*.tiff
	for i in tests/*;do echo "Traitement de l'image $$i"; ./jpeg2tiff $$i;  done

# Edition de lien des executables
jpeg2tiff : $(OBJ_FILES) $(OBJ_DIR)/
	$(LD) -o jpeg2tiff $(OBJ_FILES) $(LDFLAGS)

OBJ_FILES_HUFFMAN = $(OBJ_DIR)/test_huffman.o $(OBJ_DIR)/idct.o  $(OBJ_DIR)/conv.o $(OBJ_DIR)/iqzz.o 
OBJ_FILES_HUFFMAN += $(OBJ_DIR)/upsampler.o $(OBJ_DIR)/huffman.o $(OBJ_DIR)/unpack.o $(OBJ_DIR)/bitstream.o
OBJ_FILES_HUFFMAN += $(OBJ_DIR)/tiff.o

test_huffman : $(OBJ_FILES_HUFFMAN) $(OBJ_DIR)
	$(LD) -o test_huffman $(OBJ_FILES_HUFFMAN) $(LDFLAGS)
	./test_huffman
	rm ./test_huffman

# Compilation des sources

clean:
	rm -f jpeg2tiff $(NEW_OBJ_FILES) tests/*.tiff bin/test_huffman.o
