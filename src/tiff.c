#include "tiff.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>

struct tiff_file_desc {
        FILE *f;
        char *name;
        uint32_t blocs_h;
        uint32_t bloc_h_cour;
        uint32_t blocs_v;
        uint32_t bloc_v_cour;
        uint8_t row;
        uint32_t lignes_restantes;
        uint32_t colonnes_restantes;
        uint32_t width;
        uint32_t height;
        uint32_t *matriff;
};

void ecrire_2o(struct tiff_file_desc *tiff, uint16_t hexa) {
        // On écrit l'octet de poids fort de hexa
        fputc(hexa >> 8, tiff->f);
        // On écrit l'octet de poids faible de hexa
        fputc(hexa & 0x00FF, tiff->f);
}

void ecrire_4o(struct tiff_file_desc *tiff, uint32_t nombre) {
        // On écrit les deux octets de poids fort de nombre
        ecrire_2o(tiff, nombre >> 16);
        // On écrit les deux octet de poids faible de nombre
        ecrire_2o(tiff, nombre & 0x0000FFFF);

}

void ecrire_3o(struct tiff_file_desc *tiff, uint32_t nombre) {
        // On écrit l'octet de poids fort de nombre
        fputc(nombre >> 16, tiff->f);
        // On écrit les deux octet de poids faible de nombre
        ecrire_2o(tiff, nombre & 0x0000FFFF);

}

void ecrire_bloc_matriff(struct tiff_file_desc *tfd,
        uint32_t *mcu_rgb,
        uint32_t jmax,
        uint32_t imax,
        uint32_t nb_blocks_h,
        uint32_t indice) { //indice du premeir élément de la mcu
        for (uint32_t j = 0; j < jmax; ++j) {
                for (uint32_t i = 0; i < imax; i++) {
                        if ((tfd->matriff)[indice + i //i = colonne
                                + 8 * nb_blocks_h * j * tfd->blocs_h //j = ligne
                                + j * tfd->colonnes_restantes]
                                != 0x0) {
                                printf("ATTENTION : Réécriture de la matriff !"
                                        "%u \n",
                                        indice + i
                                        + 8 * nb_blocks_h * j * tfd->blocs_h
                                        + j * tfd->colonnes_restantes);
                        }
                        (tfd->matriff)[indice + i
                                + 8 * nb_blocks_h * j * tfd->blocs_h
                                + j * tfd->colonnes_restantes]
                                = mcu_rgb[i + 8 * nb_blocks_h * j];
                }
        }
}

struct tiff_file_desc *init_tiff_file(const char *file_name,
        uint32_t width,
        uint32_t height,
        uint32_t row_per_strip) {
        // On écrit la description du fichier dans la structure tiff
        struct tiff_file_desc *tiff = malloc(sizeof (struct tiff_file_desc));
        tiff->name = malloc(sizeof (char)*(strlen(file_name) + 1));
        strcpy(tiff->name, file_name);
        tiff->f = fopen(tiff->name, "wb"); /* b - binary mode */
        // On vérifie que l'ouverture a fonctionné
        assert(tiff->f != NULL);
        tiff->bloc_h_cour = 0;
        tiff->bloc_v_cour = 0;
        tiff->blocs_h = width / row_per_strip;
        tiff->blocs_v = height / row_per_strip;
        tiff->row = row_per_strip;
        tiff->width = width;
        tiff->height = height;

        uint32_t colonnes_restantes = width % row_per_strip;
        tiff->colonnes_restantes = colonnes_restantes;
        uint32_t lignes_restantes = height % row_per_strip;
        tiff->lignes_restantes = lignes_restantes;
        // if (colonnes_restantes != 0) {
        //         tiff->blocs_h = tiff->blocs_h +1;
        // }
        // if (lignes_restantes != 0) {
        //         tiff->blocs_v = tiff->blocs_v + 1;
        // }

        tiff->matriff = calloc(width * height, sizeof (uint32_t));
        assert(tiff->matriff != NULL);

        //Entête du fichier

        //Endianness du fichier BE, identification du TIFF (42) 
        //et adresse du premier IDF
        ecrire_4o(tiff, 0x4d4d002A);
        ecrire_4o(tiff, 0x0008);


        // On écrit le premier IFD
        // On indique qu'il y a 12 entrées dans le premier IDF
        ecrire_2o(tiff, 0x000c);
        // On spécifie le nombre de colonnes de l'image
        ecrire_2o(tiff, 0x0100); // Code largeur image
        ecrire_2o(tiff, 0x0004); // LONG
        ecrire_4o(tiff, 0x0001); // une donnée
        ecrire_4o(tiff, width); // c'est width
        // On spécifie le nombre de lignes de l'image
        ecrire_2o(tiff, 0x0101); // Code hauteur image
        ecrire_2o(tiff, 0x0004); // LONG
        ecrire_4o(tiff, 0x0001); // une donnée
        ecrire_4o(tiff, height); // c'est height
        // On donne la taille d'une composante couleur
        ecrire_2o(tiff, 0x0102); // Code BitsPerSample
        ecrire_2o(tiff, 0x0003); // SHORT
        ecrire_4o(tiff, 0x0003); // trois données
        ecrire_4o(tiff, 0x0000009E); // à l'adresse 9E se trouvent les 
        //valeurs de BitsPerSample

        // Pas de compression
        ecrire_2o(tiff, 0x0103); // Code compression
        ecrire_2o(tiff, 0x0003); // SHORT
        ecrire_4o(tiff, 0x0001); // une donnée
        ecrire_4o(tiff, 0x00010000); // pas de compression

        // Indique une image RGB
        ecrire_2o(tiff, 0x0106); // Code PhotometricInterpretation
        ecrire_2o(tiff, 0x0003); // SHORT
        ecrire_4o(tiff, 0x0001); // une donnée
        ecrire_4o(tiff, 0x00020000); // image RGB

        // Offsets des différentes lignes de l'image
        ecrire_2o(tiff, 0x0111); // Code StripOffsets
        ecrire_2o(tiff, 0x0004); // LONG
        // On fait deux cas selon si on a des blocs de 8 ou de 16 en hauteur
        if (lignes_restantes == 0) {
                //Nb de bandes
                ecrire_4o(tiff, height / row_per_strip);
                //adresse vers l'adresse de la premiere bande
                ecrire_4o(tiff, 0x000000a4 + 4 * (height / row_per_strip) + 16);
        } else {
                //nb de bandes + la bande supplémentaire
                ecrire_4o(tiff, (height / row_per_strip) + 1);
                // adresse vers la premiere bande 
                // + passage des 2 octets de sa taille
                ecrire_4o(tiff,
                        0x000000a4 + 4 * (height / row_per_strip) + 16 + 4);
        }
        // On fait des lignes de 8*width de long
        // La première est l'adresse XXXX
        //La suivante est à l'adresse XXXX + 8*width

        //En tout il y en a height/8 + des broutilles

        // Nombre de composantes par pixel
        ecrire_2o(tiff, 0x0115); // Code SamplesPerPixels
        ecrire_2o(tiff, 0x0003); // SHORT
        ecrire_4o(tiff, 0x0001); // une donnée
        ecrire_4o(tiff, 0x00030000); // 3 composantes par pixel

        // On donne le row_per_strip
        ecrire_2o(tiff, 0x116);
        ecrire_2o(tiff, 0x0004); // LONG
        ecrire_4o(tiff, 0x0001); // une donnée
        ecrire_4o(tiff, row_per_strip); // c'est row_per_strip

        // Taille en octets des différentes lignes
        ecrire_2o(tiff, 0x117);
        ecrire_2o(tiff, 0x0004); // LONG
        if (lignes_restantes == 0) {
                ecrire_4o(tiff, height / row_per_strip); // nb de bandes
        } else {
                // nb de bandes + la bande restante
                ecrire_4o(tiff, (height / row_per_strip) + 1);
        }
        ecrire_4o(tiff, 0xa4); //adresse vers les valeurs des tailles de lignes

        // Nombre de pixels par unité de mesure (horizontal)
        ecrire_2o(tiff, 0x11a);
        ecrire_2o(tiff, 0x0005); // RATIONNEL
        ecrire_4o(tiff, 0x1); //une donnée
        if (lignes_restantes == 0) {
                //adresse de la fraction
                ecrire_4o(tiff, 0xa4 + 4 * (height / row_per_strip));
        } else {
                //adresse de la fraction
                ecrire_4o(tiff, 0xa4 + 4 * (height / row_per_strip) + 4);
        }

        // Nombre de pixels par unité de mesure (vertical)
        ecrire_2o(tiff, 0x11b);
        ecrire_2o(tiff, 0x0005); // RATIONNEL
        ecrire_4o(tiff, 0x1); //une donnée
        if (lignes_restantes == 0) {
                //adresse de la fraction
                ecrire_4o(tiff, 0xac + 4 * (height / row_per_strip));
        } else {
                // adresse de la fraction + passage de l'octet de la taille
                // de la derniere ligne si elle est présente
                ecrire_4o(tiff, 0xac + 4 * (height / row_per_strip) + 4);
        }

        // Unité de mesure
        ecrire_2o(tiff, 0x128);
        ecrire_2o(tiff, 0x0003);
        ecrire_4o(tiff, 0x1);
        ecrire_4o(tiff, 0x00020000); // le centimètre

        // Offset suivant
        ecrire_4o(tiff, 0x0);

        // BitsPerSample
        ecrire_2o(tiff, 0x8);
        ecrire_2o(tiff, 0x8);
        ecrire_2o(tiff, 0x8);

        //Taille en octets des différentes bandes
        for (uint32_t i = 0; i < height / row_per_strip; i++) {
                ecrire_4o(tiff, width * row_per_strip * 3);
        }
        if (lignes_restantes != 0) {
                ecrire_4o(tiff, width * lignes_restantes * 3);
        }

        // Xresolution
        ecrire_4o(tiff, 0x64);
        ecrire_4o(tiff, 0x1);
        // Yresolution
        ecrire_4o(tiff, 0x64);
        ecrire_4o(tiff, 0x1);

        //Offsets des lignes de l'image
        for (uint32_t i = 0; i < height / row_per_strip; ++i) {
                //0x0000000a4 : offset fixe de l'entete
                //2*(height / row_per_strip) : nombre de bandes * 4octets
                //16 : passage Xresolution + Yresolution
                //4*(height/row_per_strip) : longueur de l'adressage des bandes
                //i*row_per_strip*width*3 : valeurs rgb(3octets) de chaque bande
                if (lignes_restantes == 0 && height / row_per_strip != 1) {
                        ecrire_4o(tiff,
                                0x000000a4
                                + 4 * (height / row_per_strip) + 16
                                + 4 * (height / row_per_strip)
                                + i * row_per_strip * width * 3);
                } else if (lignes_restantes != 0 && height / row_per_strip != 1) {
                        // (+ 4 octets pour indiquer la taille de la 
                        // dernière bande + 4 octets pour son offset) = +6
                        ecrire_4o(tiff, 8 + 0x000000a4
                                + 4 * (height / row_per_strip) + 16
                                + 4 * (height / row_per_strip)
                                + i * (row_per_strip * width * 3));
                } else if (lignes_restantes != 0 && height / row_per_strip == 1) {
                        ecrire_4o(tiff, 8 + 0x000000a4
                                + 4 * (height / row_per_strip) + 16
                                + 4 * (height / row_per_strip)
                                + i * (row_per_strip * width * 3));
                }
        }
        if (lignes_restantes != 0) {
                ecrire_4o(tiff, 0x000000a4 +
                        4 * (height / row_per_strip) + 16
                        + 4 * (height / row_per_strip)
                        + row_per_strip * (height / row_per_strip) * width * 3
                        + 8);
        }
        return (tiff);

}

int32_t close_tiff_file(struct tiff_file_desc *tfd) {


        for (uint32_t i = 0; i < tfd->width * tfd->height; i++) {
                ecrire_3o(tfd, tfd->matriff[i]);
                //printf("matriff[%u] = %u\n", i, tfd->matriff[i]);
        }
        free(tfd->matriff);
        free(tfd->name);
        fclose(tfd->f);
        free(tfd);

        return 0;
}

int32_t write_tiff_file(struct tiff_file_desc *tfd,
        uint32_t *mcu_rgb,
        uint8_t nb_blocks_h,
        uint8_t nb_blocks_v) {

        if (nb_blocks_v == 1 && nb_blocks_h == 1) {
                // printf("cas 1:1\n");
                //indice du premier élément de la mcu
                uint32_t indice =
                        //bandes + blocs horizontaux + ajustement si colonnes 
                        //restantes
                        64 * tfd->bloc_v_cour * tfd->blocs_h
                        + 8 * tfd->bloc_h_cour
                        + tfd->bloc_v_cour * tfd->colonnes_restantes * 8;
                //cas mcu normal
                if (tfd->bloc_v_cour != tfd->blocs_v
                        && tfd->bloc_h_cour != tfd->blocs_h) {
                        ecrire_bloc_matriff
                                (tfd, mcu_rgb, 8, 8, nb_blocks_h, indice);
                        //cas mcu en fin de bande non complete
                } else if (tfd->bloc_v_cour != tfd->blocs_v
                        && tfd->bloc_h_cour == tfd->blocs_h) {
                        ecrire_bloc_matriff
                                (tfd, mcu_rgb, 8,
                                tfd->colonnes_restantes, nb_blocks_h, indice);
                        //cas mcu en derniere bande non complete
                } else if (tfd->bloc_v_cour == tfd->blocs_v
                        && tfd->bloc_h_cour != tfd->blocs_h) {
                        ecrire_bloc_matriff
                                (tfd, mcu_rgb,
                                tfd->lignes_restantes, 8, nb_blocks_h, indice);
                        //cas mcu en derniere bande ET en fin de bande
                } else {
                        ecrire_bloc_matriff
                                (tfd, mcu_rgb, tfd->lignes_restantes,
                                tfd->colonnes_restantes, nb_blocks_h, indice);
                }
                //incrémentation de l'indice
                tfd->bloc_h_cour = tfd->bloc_h_cour + 1;
                //cas colonnes_restantes == 0
                if (tfd->bloc_h_cour == tfd->blocs_h
                        && tfd->colonnes_restantes == 0) {
                        tfd->bloc_v_cour = tfd->bloc_v_cour + 1;
                        tfd->bloc_h_cour = 0;
                }
                //cas colonne_restantes != 0
                if (tfd->bloc_h_cour == tfd->blocs_h + 1
                        && tfd->colonnes_restantes != 0) {
                        tfd->bloc_v_cour = tfd->bloc_v_cour + 1;
                        tfd->bloc_h_cour = 0;
                }

        } else if (nb_blocks_v == 1 && nb_blocks_h == 2) {
                //printf("cas 2:1\n");
                tfd->blocs_h = tfd->width / 16;
                tfd->colonnes_restantes = tfd->width % 16;
                //indice du premier élément de la mcu
                uint32_t indice = 128 * tfd->bloc_v_cour * tfd->blocs_h
                        + 16 * tfd->bloc_h_cour
                        + tfd->bloc_v_cour * tfd->colonnes_restantes * 8;
                //cas mcu normal
                if (tfd->bloc_v_cour != tfd->blocs_v
                        && tfd->bloc_h_cour != tfd->blocs_h) {
                        ecrire_bloc_matriff
                                (tfd, mcu_rgb, 8, 16, nb_blocks_h, indice);
                        //cas mcu en fin de bande non complete 
                } else if (tfd->bloc_v_cour != tfd->blocs_v
                        && tfd->bloc_h_cour == tfd->blocs_h) {
                        ecrire_bloc_matriff
                                (tfd, mcu_rgb, 8,
                                tfd->colonnes_restantes, nb_blocks_h, indice);
                        //cas mcu en derniere bande non complete
                } else if (tfd->bloc_v_cour == tfd->blocs_v
                        && tfd->bloc_h_cour != tfd->blocs_h) {
                        ecrire_bloc_matriff(tfd, mcu_rgb,
                                tfd->lignes_restantes, 16, nb_blocks_h, indice);
                        //cas mcu en derniere bande ET en fin de bande
                } else {
                        ecrire_bloc_matriff
                                (tfd, mcu_rgb, tfd->lignes_restantes,
                                tfd->colonnes_restantes, nb_blocks_h, indice);
                }
                //incrémentation de l'indice
                tfd->bloc_h_cour = tfd->bloc_h_cour + 1;
                //cas colonne_restantes == 0
                if (tfd->bloc_h_cour == tfd->blocs_h
                        && tfd->colonnes_restantes == 0) {
                        tfd->bloc_v_cour = tfd->bloc_v_cour + 1;
                        tfd->bloc_h_cour = 0;
                }
                //cas colonne_restantes != 0
                if (tfd->bloc_h_cour == tfd->blocs_h + 1
                        && tfd->colonnes_restantes != 0) {
                        tfd->bloc_v_cour = tfd->bloc_v_cour + 1;
                        tfd->bloc_h_cour = 0;
                }

        } else if (nb_blocks_v == 2 && nb_blocks_h == 2) {
                //printf("2:2\n");
                //indice du premier élément de la mcu
                uint32_t indice = 256 * tfd->bloc_v_cour * tfd->blocs_h
                        + 16 * tfd->bloc_h_cour
                        + tfd->bloc_v_cour * tfd->colonnes_restantes * 16;
                //cas mcu normal
                if (tfd->bloc_v_cour != tfd->blocs_v
                        && tfd->bloc_h_cour != tfd->blocs_h) {
                        ecrire_bloc_matriff
                                (tfd, mcu_rgb, 16, 16, nb_blocks_h, indice);
                        //cas mcu en fin de bande non complete 
                } else if (tfd->bloc_v_cour != tfd->blocs_v
                        && tfd->bloc_h_cour == tfd->blocs_h) {
                        ecrire_bloc_matriff(tfd, mcu_rgb, 16,
                                tfd->colonnes_restantes, nb_blocks_h, indice);
                        //cas mcu en derniere bande non complete
                } else if (tfd->bloc_v_cour == tfd->blocs_v
                        && tfd->bloc_h_cour != tfd->blocs_h) {
                        ecrire_bloc_matriff(tfd, mcu_rgb,
                                tfd->lignes_restantes, 16, nb_blocks_h, indice);
                        //cas mcu en derniere bande ET en fin de bande
                } else {
                        ecrire_bloc_matriff(tfd, mcu_rgb, tfd->lignes_restantes,
                                tfd->colonnes_restantes, nb_blocks_h, indice);
                }
                //incrémentation de l'indice
                tfd->bloc_h_cour = tfd->bloc_h_cour + 1;
                //cas colonne_restantes == 0
                if (tfd->bloc_h_cour == tfd->blocs_h
                        && tfd->colonnes_restantes == 0) {
                        tfd->bloc_v_cour = tfd->bloc_v_cour + 1;
                        tfd->bloc_h_cour = 0;
                }
                //cas colonne_restantes != 0
                if (tfd->bloc_h_cour == tfd->blocs_h + 1
                        && tfd->colonnes_restantes != 0) {
                        tfd->bloc_v_cour = tfd->bloc_v_cour + 1;
                        tfd->bloc_h_cour = 0;
                }

        } else {
                printf("Erreur  : cas de mcu non géré\n");
        }

        return 0;
}