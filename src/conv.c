#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#define constante1 0.0009267
#define constante2 1.4016868
#define constante3 0.3436954
#define constante4 0.7141690
#define constante5 1.7721604
#define constante6 0.0009902


/*R = Y - cst1(Cb - 128) + cst2( Cr - 128)
  G = Y - cst3(Cb - 128) - cst4(Cr - 128)
  B = Y + cst5(Cb - 128) + cst6(Cr - 128)
*/





extern void YCbCr_to_ARGB(uint8_t  *mcu_YCbCr[3], uint32_t *mcu_RGB,
		uint32_t nb_blocks_h, uint32_t nb_blocks_v) {
	/* on crée les vecteurs R, G et B, qu'on initialise à 0*/
	int32_t rouge =0;
	int32_t vert =0;
	int32_t bleu =0;
	for ( uint32_t i = 0; i < 64 * nb_blocks_h * nb_blocks_v; i++ ) {
		/* on calcule R à l'aide de la formule 
		R = Y - 0,0009267(Cb - 128) + 1,4016868(Cr - 128)*/
		rouge = mcu_YCbCr[0][i] - constante1*(mcu_YCbCr[1][i] - 128) 
			+ constante2*(mcu_YCbCr[2][i] - 128);
		/* on s'assure que rouge est bien dans le bon intervalle*/
		if (rouge > 255) {
			rouge = 255;
		}
		if (rouge < 0 ) {
			rouge = 0;
		}
		/* on décale R car il est codé sur le 3eme octet de mcu_RGB*/
		rouge = rouge << 16;
		/* on calcule G à l'aide de la formule 
		G = Y - 0,3436954(Cb-128) + 0,7141690(Cr - 128)*/
		vert = mcu_YCbCr[0][i] - constante3*(mcu_YCbCr[1][i] - 128)
		      - constante4*(mcu_YCbCr[2][i] -128);
		/* on s'assure que vert est bien dans le bon intervalle*/
		if (vert > 255) {
			vert = 255;
		}
		if (vert < 0) {
			vert = 0;
		}
		/* on décale vert car il est codé sur le 2nd octet de mcu_RGB*/
		vert = vert << 8;
		/* on calcule B à l'aide de la formule 
		B = Y + 1,7721604( Cb - 128) + 0,0009902(Cr - 128)*/
		bleu = mcu_YCbCr[0][i] + constante5*(mcu_YCbCr[1][i] - 128) 
		      + constante6*(mcu_YCbCr[2][i] - 128);
		/* on s'assure que bleu est bien dans le bon intervalle*/
		if (bleu > 255) {
			bleu = 255;
		}
		if (bleu < 0) {
			bleu = 0;
		}
		/* on initialise mcu_RGB à 0*/
		mcu_RGB[i] = 0;
		/* on calcule mcu_RGB*/
		mcu_RGB[i] = rouge + vert + bleu;
	}
};


