#include "conv.h"
#include "idct.h"
#include "iqzz.h"
#include "unpack.h"
#include "upsampler.h"
#include "huffman.h"
#include "bitstream.h"
#include "tiff.h"
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

bool verif_bitstream(uint32_t dest,
        uint32_t ref,
        uint8_t nbbitlu,
        uint8_t longueurref) {
        if (longueurref != nbbitlu) {
                fprintf(stderr,"on a lu %u bits au lieu de %u\n", nbbitlu, longueurref);
                return false;
        }
        if (dest != ref) {
                fprintf(stderr,"on attendait %x, on a lu %x\n", ref, dest);
                return false;
        }
        return true;
}

bool lit_et_verif_bitstream(uint32_t *dest,
        uint32_t ref,
        uint8_t nbbitlu,
        uint8_t longueurref,
        struct bitstream *stream,
        bool bytestuffing) {
        nbbitlu = read_bitstream(stream, longueurref, dest, bytestuffing);
        return verif_bitstream(*dest, ref, nbbitlu, longueurref);
}

void liberer_memoire(char *chartmp,
        struct huff_table *ptr_huff00,
        struct huff_table *ptr_huff01,
        struct huff_table *ptr_huff10,
        struct huff_table *ptr_huff11,
        uint8_t *upsampler0,
        uint8_t *upsampler1,
        uint8_t *upsampler2,
        uint32_t *RGB,
        struct bitstream *stream) {
        //on libère le char sortie dont on avait eu besoin pour le nom
        free(chartmp);

        //on libère les tables de huffman
        free_huffman_table(ptr_huff00);
        free_huffman_table(ptr_huff01);
        free_huffman_table(ptr_huff10);
        free_huffman_table(ptr_huff11);

        //on libère les tableaux qu'on a alloué
        free(upsampler0);
        free(upsampler1);
        free(upsampler2);
        free(RGB);

        //on libère le bitstream
        free_bitstream(stream);
}

int main(int argc, char *argv[]) {
        uint8_t nb_blocks_in_h[3], nb_blocks_in_v[3], iQ[3];
        uint8_t nb_blocks_out_h, nb_blocks_out_v;
        uint8_t nbbitlu = 0;
        uint32_t dest;
        int32_t tableau_out_unpack[3][64];
        int32_t tableau_out_iqzz[3][64];
        uint8_t tableau_out_idct[3][64];
        uint32_t *tabmcu_RGB;
        uint32_t largeur;
        uint32_t hauteur;
        uint32_t row_per_strip;

        //ATTENTION: Bien compter le nbr d'arguments: doit valoir 1
        if (argc != 2) {
                fprintf(stderr,"Usage: ./jpeg2tiff <jpeg_file> \n");
                return 2;
        }
        fprintf(stderr,"[*");
        /*creation du bitstream */
        struct bitstream *stream = create_bitstream(argv[1]);
        /*on cherche le début du fichier*/
        if (!lit_et_verif_bitstream(&dest, 0xFF, nbbitlu, 8, stream, 0)) {
                return -1;
        }
        if (!lit_et_verif_bitstream(&dest, 0xd8, nbbitlu, 8, stream, 0)) {
                return -1;
        }
        // On entre dans la zone marqueur JFIF
        if (!lit_et_verif_bitstream(&dest, 0xFF, nbbitlu, 8, stream, 0)) {
                return -1;
        }
        if (!lit_et_verif_bitstream(&dest, 0xE0, nbbitlu, 8, stream, 0)) {
                return -1;
        }
        // Longueur de la section JFIF 
        read_bitstream(stream, 8, &dest, 0);
        read_bitstream(stream, 8, &dest, 0);


        // On vérifie que le symbole est bien JFIF\0
        if (!lit_et_verif_bitstream(&dest, 0x4A, nbbitlu, 8, stream, 0)) {
                return -1;
        }
        if (!lit_et_verif_bitstream(&dest, 0x46, nbbitlu, 8, stream, 0)) {
                return -1;
        }
        if (!lit_et_verif_bitstream(&dest, 0x49, nbbitlu, 8, stream, 0)) {
                return -1;
        }
        if (!lit_et_verif_bitstream(&dest, 0x46, nbbitlu, 8, stream, 0)) {
                return -1;
        }
        if (!lit_et_verif_bitstream(&dest, 0x0, nbbitlu, 8, stream, 0)) {
                return -1;
        }
        // On cherche le prochain marqueur
        skip_bitstream_until(stream, 0xFF);
        if (!lit_et_verif_bitstream(&dest, 0xFF, nbbitlu, 8, stream, 0)) {
                return -1;
        }
        //on lit le prochain marqueur
        assert(read_bitstream(stream, 8, &dest, 0) == 8);
        //on passe les commentaires
        while (dest != 0xDB) {
                skip_bitstream_until(stream, 0xFF);
                if (!lit_et_verif_bitstream(&dest, 0xFF, nbbitlu, 8, stream, 0)) {
                        return -1;
                }
                //on lit le prochain marqueur
                assert(read_bitstream(stream, 8, &dest, 0) == 8);
        }
        // Au maximum trois tables de quantification
        uint8_t * quantif[3] = {NULL, NULL, NULL};
        uint8_t i;
        // On a le cas où les tables de quantification sont définies avec 
        //plusieurs marqueurs DB
        while (dest == 0xDB) {
                // On passe la longueur de la section, différente selon 
                // chaque image
                uint16_t longueur_section_DQT;
                assert(read_bitstream(stream, 8, &dest, 0) == 8);
                longueur_section_DQT = dest;
                longueur_section_DQT = longueur_section_DQT << 8;
                assert(read_bitstream(stream, 8, &dest, 0) == 8);
                // Si la longueur de la section est plus grande que 0x43, alors 
                // il y a plusieurs tables de quantification sous le même 
                // marqueur DB
                longueur_section_DQT = longueur_section_DQT | dest;
                while (longueur_section_DQT >= 0x43) {
                        // On passe la précision de la table de quantification
                        assert(read_bitstream(stream, 4, &dest, 0) == 4);
                        // Indice iQ de la table de quantification
                        assert(read_bitstream(stream, 4, &dest, 0) == 4);
                        i = dest;
                        quantif[i] = malloc(sizeof (uint8_t)*64);

                        for (int j = 0; j < 64; ++j) {
                                // Lire et stocker dans quantif les valeurs de 
                                // la table de quantification
                                assert(read_bitstream(stream, 8, &dest, 1) == 8);
                                quantif[i][j] = (uint8_t) dest;
                        }
                        // regarder les autres tables de quantification
                        longueur_section_DQT = longueur_section_DQT - 0x41;
                }
                //parfois la section peut-être plus longue, on saute à FF
                skip_bitstream_until(stream, 0xFF);
                if (!lit_et_verif_bitstream(&dest, 0xFF, nbbitlu, 8, stream, 0)) {
                        return -1;
                }
                assert(read_bitstream(stream, 8, &dest, 0) == 8);
        }

        fprintf(stderr,"*");
        // On est sur le début de l'image
        assert(dest = 0xC0);

        // On passe la longueur de la section (sur 2o) qui suit le marqueur SOF 
        // dans dest
        assert(read_bitstream(stream, 8, &dest, 0) == 8);
        assert(read_bitstream(stream, 8, &dest, 0) == 8);


        // On met la précision dans dest
        assert(read_bitstream(stream, 8, &dest, 0) == 8);


        // On range la hauteur (sur 2 octets) dans hauteur
        assert(read_bitstream(stream, 8, &hauteur, 0) == 8);
        hauteur = hauteur << 8;
        assert(read_bitstream(stream, 8, &dest, 0) == 8);
        hauteur = hauteur | dest;
        // On range la largeur (sur 2 octets) dans largeur
        assert(read_bitstream(stream, 8, &largeur, 0) == 8);
        largeur = largeur << 8;
        assert(read_bitstream(stream, 8, &dest, 0) == 8);
        largeur = largeur | dest;
        // On vérifie que l'on est en YCbCr
        if (!lit_et_verif_bitstream(&dest, 0x03, nbbitlu, 8, stream, 0)) {
                fprintf(stderr,"L'image JPEG n'est pas encodée en YCbCr\n");
                return -1;
        }
        // On lit l'indice de composante iC
        assert(read_bitstream(stream, 8, &dest, 0) == 8);
        while (dest != 0xFF) {
                uint8_t i = dest - 1;
                assert(read_bitstream(stream, 4, &dest, 0) == 4);
                nb_blocks_in_h[i] = dest; //facteur d'échantillonage horizontal
                assert(read_bitstream(stream, 4, &dest, 0) == 4);
                nb_blocks_in_v[i] = dest; //facteur d'échantillonage vertical
                assert(read_bitstream(stream, 8, &dest, 0) == 8);
                iQ[i] = dest;
                // On lit l'indice de composante iC
                assert(read_bitstream(stream, 8, &dest, 0) == 8);
        }
        // On entre dans la partie DHT
        if (!lit_et_verif_bitstream(&dest, 0xC4, nbbitlu, 8, stream, 0)) {
                return -1;
        }
        fprintf(stderr,"*");
        struct huff_table * ptr_tab_huff[2][2];
        while (dest == 0xC4) {
                //ATTENTION: peut-être pb car plsr tables sous le mê marqueur
                // On passe la longueur de la section (sur 2o) qui suit le 
                // marqueur DHT dans dest
                assert(read_bitstream(stream, 8, &dest, 0) == 8);
                assert(read_bitstream(stream, 8, &dest, 0) == 8);
                if (!lit_et_verif_bitstream(&dest, 0x0, nbbitlu, 3, stream, 0)) {
                        fprintf(stderr,"\nOn s'attend à avoir 3 bits à 0 avant les "
                                "informations sur la table de HUFFMAN\n");
                        return -1;
                }
                assert(read_bitstream(stream, 1, &dest, 0) == 1); // AC ou DC

                uint8_t ACDC = dest;
                // Indice de la table de HUFFMAN
                assert(read_bitstream(stream, 4, &dest, 0) == 4);
                uint8_t indice = dest;
                assert(indice <= 3);

                uint16_t nb_byte_read;
                ptr_tab_huff[ACDC][indice]
                        = load_huffman_table(stream, &nb_byte_read);
                if (!lit_et_verif_bitstream(&dest, 0xFF, nbbitlu, 8, stream, 0)) {
                        return -1;
                }
                assert(read_bitstream(stream, 8, &dest, 0) == 8);
        }

        //on rentre dans la section Start Of Scan (SOS)
        if (dest != 0xDA) {
                return -1;
        }
        //on passe la longueur de la section sur deux octets
        assert(read_bitstream(stream, 8, &dest, 0) == 8);
        assert(read_bitstream(stream, 8, &dest, 0) == 8);
        // nombre de composantes, on ne gère que le cas avec les composantes 
        // YCbCr
        assert(read_bitstream(stream, 8, &dest, 0) == 8);

        uint8_t i_DC[3];
        uint8_t i_AC[3];
        uint8_t iC;

        for (int i = 0; i < 3; ++i) {
                // On range les les indices de table de Huffman selon la 
                // composante iC
                assert(read_bitstream(stream, 8, &dest, 0) == 8);
                iC = dest;
                //fprintf(stderr,"iC : %u\n", iC);
                assert(read_bitstream(stream, 4, &dest, 0) == 4);
                i_DC[iC - 1] = dest;
                assert(read_bitstream(stream, 4, &dest, 0) == 4);
                i_AC[iC - 1] = dest;
        }
        // Début de la sélection (non utilisé)
        assert(read_bitstream(stream, 8, &dest, 0) == 8);
        // Fin de la sélection (non utilisé)
        assert(read_bitstream(stream, 8, &dest, 0) == 8);

        assert(read_bitstream(stream, 4, &dest, 0) == 4); // Ah
        assert(read_bitstream(stream, 4, &dest, 0) == 4); // Al

        int32_t pred_DC[3] = {0, 0, 0};
        // Cas (a) : sans sous échantillonage
        if (nb_blocks_in_v[0] == 1
                && nb_blocks_in_h[0] == 1
                && nb_blocks_in_v[1] == 1
                && nb_blocks_in_h[1] == 1
                && nb_blocks_in_v[2] == 1
                && nb_blocks_in_h[2] == 1) {
                nb_blocks_out_h = 1;
                nb_blocks_out_v = 1;
        }// Cas (b) : sous échantillonage horizontal (4:2:2)
        else if (nb_blocks_in_v[0] == 1
                && nb_blocks_in_h[0] == 2
                && nb_blocks_in_v[1] == 1
                && nb_blocks_in_h[1] == 1
                && nb_blocks_in_v[2] == 1
                && nb_blocks_in_h[2] == 1) {
                nb_blocks_out_h = 2;
                nb_blocks_out_v = 1;
        }// Cas (c) : sous échantillonage horizontal et vertical (4:2:0)
        else if (nb_blocks_in_v[0] == 2
                && nb_blocks_in_h[0] == 2
                && nb_blocks_in_v[1] == 1
                && nb_blocks_in_h[1] == 1
                && nb_blocks_in_v[2] == 1
                && nb_blocks_in_h[2] == 1) {
                nb_blocks_out_h = 2;
                nb_blocks_out_v = 2;
        } else {
                fprintf(stderr,"Echantillonage autre que "
                        "1x1, 1x2 et 2x2 non géré.\n");
                return -1;
        }

        //on crée les variables et on alloue la place dans la mémoire
        row_per_strip = 8 * nb_blocks_out_v;

        // on transforme le nom du fichier d'entrée nom.jpeg en fichier de 
        // sortie nom.tiff
        char *entree = argv[1];
        char *point = strchr(entree, '.');
        if (point == NULL) {
                fprintf(stderr,"il manque le format jpeg, impossible");
        }
        //calcule la distance entre le début de l'argument d'entrée et le .
        uint8_t taille = point - entree;
        //+6 à cause de .tiff + \0
        char *sortie = malloc(sizeof (char)*(taille + 6));
        for (uint8_t c = 0; c < taille; c++) {
                sortie[c] = 'a'; //lettre au hasard
        }
        sortie[taille] = '\0';
        sortie = strncpy(sortie, entree, taille);
        char *nomfichiersortie = strcat(sortie, ".tiff");

        struct tiff_file_desc *fichiersortie
                = init_tiff_file(nomfichiersortie, largeur, hauteur,
                row_per_strip);
        uint8_t * tableau_out_upsampler[3];
        tableau_out_upsampler[0]
                = malloc(sizeof (uint8_t)*64 * nb_blocks_out_h * nb_blocks_out_v);
        tableau_out_upsampler[1]
                = malloc(sizeof (uint8_t)*64 * nb_blocks_out_h * nb_blocks_out_v);
        tableau_out_upsampler[2]
                = malloc(sizeof (uint8_t)*64 * nb_blocks_out_h * nb_blocks_out_v);
        tabmcu_RGB
                = malloc(sizeof (uint32_t)*64 * nb_blocks_out_h * nb_blocks_out_v);

        //on calcule le nombre de blocs de la MCU qu'on a en largeur
        uint32_t nb_blocs_largeur;
        if (largeur % (nb_blocks_out_h * 8) == 0) {
                nb_blocs_largeur = largeur / (nb_blocks_out_h * 8);
        } else {
                nb_blocs_largeur = largeur / (nb_blocks_out_h * 8) + 1;
        }
        //on calcule le nombre de blocs de la MCU qu'on a en hauteur
        uint32_t nb_blocs_hauteur;
        if (hauteur % (nb_blocks_out_v * 8) == 0) {
                nb_blocs_hauteur = hauteur / (nb_blocks_out_v * 8);
        } else {
                nb_blocs_hauteur = hauteur / (nb_blocks_out_v * 8) + 1;
        }
        uint32_t nb_blocs = nb_blocs_hauteur * nb_blocs_largeur;
        //on fait le processus de décodage sur tous les blocs de la MCU
        for (uint32_t lh = 0; lh < nb_blocs; ++lh) {
                //on le fait d'abord pour Y
                uint8_t *tableau_in_upsampler
                        = malloc(sizeof (uint8_t)*64 * nb_blocks_in_h[0]
                        * nb_blocks_in_v[0]);
                for (int y = 0; y < nb_blocks_in_v[0] * nb_blocks_in_h[0]; ++y) {
                        unpack_block(stream,
                                ptr_tab_huff[0][i_DC[0]], &(pred_DC[0]),
                                ptr_tab_huff[1][i_AC[0]],
                                tableau_out_unpack[0]);
                        iqzz_block(tableau_out_unpack[0], tableau_out_iqzz[0],
                                quantif[iQ[0]]);
                        idct_block(tableau_out_iqzz[0], tableau_out_idct[0]);
                        // on concatène tous les tableaux de chaque blocs pour 
                        // en avoir un unique pour Y
                        for (int u = 0; u < 64; ++u) {
                                tableau_in_upsampler[u + 64 * y]
                                        = tableau_out_idct[0][u];
                        }
                }
                upsampler(tableau_in_upsampler, nb_blocks_in_h[0],
                        nb_blocks_in_v[0], tableau_out_upsampler[0],
                        nb_blocks_out_h, nb_blocks_out_v);
                free(tableau_in_upsampler);

                //on gère le cas Cb et Cr
                for (int i = 1; i < 3; ++i) {
                        unpack_block(stream,
                                ptr_tab_huff[0][i_DC[i]], &(pred_DC[i]),
                                ptr_tab_huff[1][i_AC[i]],
                                tableau_out_unpack[i]);
                        iqzz_block(tableau_out_unpack[i], tableau_out_iqzz[i],
                                quantif[iQ[i]]);
                        idct_block(tableau_out_iqzz[i], tableau_out_idct[i]);
                        uint8_t *tableau_in_upsampler
                                = malloc(sizeof (uint8_t)*64 * nb_blocks_in_h[i]
                                * nb_blocks_in_v[i]);
                        // on concatène tous les tableaux de chaque blocs pour 
                        // en avoir un unique pour Cb et Cr
                        for (int k = 0; k < 64 * nb_blocks_in_h[i]
                                * nb_blocks_in_v[i]; ++k) {
                                tableau_in_upsampler[k] = tableau_out_idct[i][k];
                        }
                        upsampler(tableau_in_upsampler, nb_blocks_in_h[i],
                                nb_blocks_in_v[i], tableau_out_upsampler[i],
                                nb_blocks_out_h, nb_blocks_out_v);
                        free(tableau_in_upsampler);
                }
                // on sort des cas Y Cb et Cr car on ne fait la fonction 
                // YCbCr_to_ARGB qu'une fois 
                YCbCr_to_ARGB(tableau_out_upsampler, tabmcu_RGB,
                        nb_blocks_out_h, nb_blocks_out_v);

                /*partie tiff: écriture dans le fichier de sortie*/
                //enlever le .jpeg de argv[1]
                write_tiff_file(fichiersortie, tabmcu_RGB,
                        nb_blocks_out_h, nb_blocks_out_v);
                // Affichage de la progression
                if (nb_blocs > 20) {
                        if (lh % (nb_blocs/20) == 0) {
                                 fprintf(stderr,"*");
                        }
                }
                        
        }
        for (uint8_t i = 0; i < 3; i++) {
                free(quantif[i]);
        }
        //on ferme le fichier tiff
        close_tiff_file(fichiersortie);

        //on lit la fin du fichier
        skip_bitstream_until(stream, 0xFF);
        if (!lit_et_verif_bitstream(&dest, 0xFF, nbbitlu, 8, stream, 0)) {
                return -1;
        }
        if (!lit_et_verif_bitstream(&dest, 0xD9, nbbitlu, 8, stream, 0)) {
                return -1;
        }

        liberer_memoire(sortie, ptr_tab_huff[0][0], ptr_tab_huff[0][1],
                ptr_tab_huff[1][0], ptr_tab_huff[1][1],
                tableau_out_upsampler[0], tableau_out_upsampler[1],
                tableau_out_upsampler[2], tabmcu_RGB, stream);
        fprintf(stderr,"*]\n");

        return 0;
}
