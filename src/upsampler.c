#include "upsampler.h"
#include <math.h>
#include <assert.h>

void upsampler(uint8_t *in,
        uint8_t nb_blocks_in_h, uint8_t nb_blocks_in_v,
        uint8_t *out,
        uint8_t nb_blocks_out_h, uint8_t nb_blocks_out_v) {
        if ((nb_blocks_in_h == 1) & (nb_blocks_in_v == 1)) {
                if ((nb_blocks_out_h == 1) & (nb_blocks_out_v == 1)) {
                        for (int i = 0; i < 64; i++) {
                                out[i] = in[i];
                        }
                } else if ((nb_blocks_out_h == 2) & (nb_blocks_out_v == 1)) {
                        for (int i = 0; i < 64; i++) {
                                int ligne = i / 8;
                                int mod = i % 8;
                                out[16 * ligne + 2 * mod] = in[i];
                                out[16 * ligne + 2 * mod + 1] = in[i];
                        }
                } else if ((nb_blocks_out_h == 2) & (nb_blocks_out_v == 2)) {
                        for (int i = 0; i < 64; i++) {
                                int ligne = i / 8;
                                int mod = i % 8;
                                out[16 * 2 * ligne + 2 * mod] = in[i];
                                out[16 * 2 * ligne + 2 * mod + 1] = in[i];
                                out[16 * 2 * ligne + 16 + 2 * mod] = in[i];
                                out[16 * 2 * ligne + 16 + 2 * mod + 1] = in[i];
                        }
                }
        } else if ((nb_blocks_in_h == 2) & (nb_blocks_in_v == 2)) {
                for (int i = 0; i < 8; i++) {
                        for (int j = 0; j < 8; j++) {
                                out[2 * j * 8 + i] = in[j * 8 + i];
                                out[(2 * j + 1) * 8 + i] = in[64 + j * 8 + i];
                                out[128 + 2 * j * 8 + i] = in[128 + j * 8 + i];
                                out[128 + (2 * j + 1) * 8 + i] 
                                        = in[128 + 64 + j * 8 + i];
                        }
                }
        } else if ((nb_blocks_in_h == 2) & (nb_blocks_in_v == 1)) {
                if ((nb_blocks_out_h == 2) & (nb_blocks_out_v == 1)) {
                        for (int i = 0; i < 8; i++) {
                                for (int j = 0; j < 8; j++) {
                                        out[2 * j * 8 + i] = in[j * 8 + i];
                                        out[(2 * j + 1) * 8 + i] 
                                                = in[64 + j * 8 + i];
                                }
                        }
                }
        } else {
                printf("Cas de MCU non géré\n");
                assert(0);
        }
}