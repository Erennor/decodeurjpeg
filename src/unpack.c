#include "unpack.h"
#include <math.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

int32_t unpack_DC(struct bitstream *stream,
        struct huff_table *table_DC,
        int32_t *pred_DC) {
        // On prend une variable m pour la classe de magnitude(tient sur 4 bits)
        int8_t m, m_read;
        // On récupère la valeur next de taille m bits
        uint32_t next;
        int16_t valeur_diff;
        // On lit la valeur de magnitude courante, 
        // elle correspond au premier mot de code du bloc
        m = next_huffman_value(table_DC, stream);
        // On vérifie que m est plus petit strictement que 11
        assert(m <= 11);
        // On écrit dans next la valeur décodée
        m_read = read_bitstream(stream, m, &next, 1);
        // On vérifie que le nombre de bits lu (m_read) est bien 
        // égal à la magnitude m;
        assert(m == m_read);
        // On fait correspondre à valeur_diff la valeur de la différence 
        // par rapport au bloc précédent
        valeur_diff = valeur_selon_magnitude(m, next);
        *pred_DC = *pred_DC + valeur_diff;
        return *pred_DC;
}

void unpack_AC(struct bitstream *stream,
        struct huff_table *table_AC,
        int32_t bloc[64]) {

        int8_t i = 0;
        uint32_t zeros;
        // On prend une variable huff pour la concaténation des zéros précédents
        //et la classe de magnitude (tient sur 8 bits)
        uint8_t huff;
        // On prend une variable m pour la classe de magnitude(tient sur 4 bits)
        uint8_t m;
        // On prend une variable m_read pour vérifier le nombre de bits 
        //lus dans le flux
        uint8_t m_read;
        // On récupère la valeur next de taille m bits
        uint32_t next;
        // tant que l'on a pas rempli tout le vecteur
        while (i < 63) {
                // On lit les zeros précédents et la valeur de magnitude
                huff = next_huffman_value(table_AC, stream);
                // Si on détecte la fin du bloc
                if (huff == 0x00) {
                        // Alors on sort
                        return;
                }// Sinon ce n'est pas la fin du bloc
                else {
                        // On récupère les quatre bits de poids fort : 
                        // c'est le nombre de zéros qui précèdent la valeur 
                        // à décoder
                        zeros = huff >> 4;
                        // On décale l'indice du nombre de zéros
                        i = i + zeros + 1;
                        // Il y a erreur trop de cases à remplir
                        assert(i < 64);
                        // On récupère les quatre bits de poids faible : 
                        //c'est la magnitude m
                        m = huff & 0x0F;
                        // Si m est nul c'est qu'il y a un 16ème zéro
                        if (m == 0) {
                                // On vérifie que l'on est pas dans le cas 0x?0,
                                // qui est interdit
                                assert(zeros == 15);
                                // Et on laisse des zéros dans la case suivante
                                // du bloc
                        } else {
                                // On vérifie que m est plus petit strictement
                                // que 10
                                assert(m <= 10);
                                // On écrit dans next la valeur du coefficient 
                                //AC décodée
                                m_read = read_bitstream(stream, m, &next, 1);
                                // On vérifie que le nombre de bits lu (m_read)
                                // est bien égal à la magnitude m
                                assert(m == m_read);
                                // On écrit la composante AC si on a pas 
                                // 16 composantes nulles
                                bloc[i] = valeur_selon_magnitude(m, next);
                        }
                }
        }
        // On ne doit pas dépasser la taille du bloc + 1
        assert(i < 65);
}

void unpack_block(struct bitstream *stream,
        struct huff_table *table_DC,
        int32_t *pred_DC,
        struct huff_table *table_AC,
        int32_t bloc[64]) {
        // On remplit le bloc avec des zeros
        for (int i = 0; i < 64; ++i) {
                bloc[i] = 0;
        }
        bloc[0] = unpack_DC(stream, table_DC, pred_DC);
        unpack_AC(stream, table_AC, bloc);
}
