#include "huffman.h"
#include "bitstream.h"
#include <stdio.h>
#include <stdlib.h>

/* on utilise l'arbre du sujet. On utilise le code
	a = 1
	b = 2
	c = 3
	d = 4
	e = 5 */

int main (void) {

	int8_t testA;
	int8_t testB;
	int8_t testC;
	int8_t testD;
	int8_t testE;
	
	struct bitstream *stream;
	
	struct huff_table *arbre = NULL;
	/*on insère le d*/
	uint32_t compteurlongueur = 0;
	inserer_arbre (&arbre, 4, 2, compteurlongueur);
	/*on insère le e*/
	compteurlongueur = 0;
	inserer_arbre (&arbre, 5, 2, compteurlongueur);
	/*on insère le a */
	compteurlongueur = 0;
	inserer_arbre(&arbre, 1, 2, compteurlongueur);
	/*on insère le c */
	compteurlongueur = 0;
	inserer_arbre(&arbre, 3,3, compteurlongueur);
	/*on insère le b */
	compteurlongueur = 0;
	inserer_arbre(&arbre, 2, 3, compteurlongueur);
	
	/*on doit avoir codeA = 10
			codeB = 111
			codeC = 110
			codeD = 00
			codeE = 01*/
	
	stream = create_bitstream("tests_huffman/testhuffman.jpeg");
	
	testA = next_huffman_value(arbre, stream);
	testB = next_huffman_value(arbre, stream);
	testC = next_huffman_value(arbre, stream);
	testD = next_huffman_value(arbre, stream);
	testE = next_huffman_value(arbre, stream); 
	
	printf("la val de A vaut 1, on a %i\n", testA);
	printf("la val de B vaut 2, on a %i\n", testB);
	printf("la val de C vaut 3, on a %i\n", testC);
	printf("la val de D vaut 4, on a %i\n", testD);
	printf("la val de E vaut 5, on a %i\n", testE);
	return 0;
	
	
}
