#include "bitstream.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

struct bitstream {
        FILE *fichier;
        //taille du fichier en octet
        uint32_t taille_totale;
        // position de la tête de lecture du fichier
        uint32_t curseur_fichier;
        // position sur le ième bit de l'octet lu
        uint8_t position;
        // tete est l'octet lu
        uint8_t tete;
        bool jailuFF;
};

void tete_sur_octet_suiv(struct bitstream *stream, bool byte_stuffing) {
        if (stream->tete == 0xFF && byte_stuffing == true) {
                stream->jailuFF = true;
                stream->tete = fgetc(stream->fichier);
                stream->curseur_fichier = stream->curseur_fichier + 1; 
        }
        else if (stream->tete == 0x00 && stream->jailuFF && byte_stuffing == true) {
                stream->jailuFF = false;
                // On lit le prochain octet 
                stream->tete = fgetc(stream->fichier);
                stream->curseur_fichier = stream->curseur_fichier + 1;
        }
        else {
                stream->tete = fgetc(stream->fichier); 
                stream->curseur_fichier = stream->curseur_fichier + 1; 
        }
}


struct bitstream *create_bitstream(const char *filename) {

        // On alloue la structure bitstream
        struct bitstream *stream = malloc(sizeof (struct bitstream));
        stream->fichier = fopen(filename, "rb"); // ouverture en lecture binaire 
        // On vérifie que le fichier a bien été ouvert
        if (stream->fichier == NULL) {
                printf("Erreur : fichier non lu");
                return NULL;
        }
        assert(stream != NULL);
        // On calcule la taille totale du fichier
        fseek(stream->fichier, 0, SEEK_END);
        stream->taille_totale = (uint32_t) ftell(stream->fichier) + 1;
        // On se place au début du fichier
        fseek(stream->fichier, 0, SEEK_SET);
        // On se place sur le premier octet
        // tete_sur_octet_suiv(stream, false);
        // On se place sur le premier octet, premier bit du stream
        stream->curseur_fichier = 0;
        stream->position = 0;
        stream->jailuFF = false;
        stream->tete = 0xFF;
        tete_sur_octet_suiv(stream, false);
        // On renvoit le pointeur vers la structure crée
        return stream;
}

bool end_of_bitstream(struct bitstream *stream) {

        /* On regarde si le curseur ne dépasse pas 
         * la longueur totale du stream */
        if (8 * stream->curseur_fichier + stream->position
                <= 8 * stream->taille_totale) {
                return false;
        } else {
                printf("Erreur : taille du flux dépassée");
                assert(8 * stream->curseur_fichier + stream->position
                        > 8 * stream->taille_totale);
                return true;
        }
}


uint8_t read_bitstream(struct bitstream *stream,
        uint8_t nb_bits,
        uint32_t *dest,
        bool byte_stuffing) {
        uint8_t nb_octets = nb_bits / 8;
        uint8_t nb_bits_restants = nb_bits % 8;
        uint8_t nb_bits_lus = 0;
        if (stream->tete == 0xFF && byte_stuffing == true) {

                stream->jailuFF = true;
        }
        if (stream->tete == 0x00 && stream->jailuFF && byte_stuffing == true) {
                stream->jailuFF = false;
                // On lit le prochain octet 
                stream->tete = fgetc(stream->fichier);
                stream->curseur_fichier = stream->curseur_fichier + 1;
        }
        // on vérifie que l'on ne demande pas de lire + de 32 bits
        // car dest est de taille 32
        assert(nb_bits < 33);
        // On commence par lire l'octet indiqué par le curseur
        *dest = stream->tete;
        // On masque les bits déjà lus précédemment
        *dest = *dest & (0b11111111 >> (stream->position));
        if (nb_octets == 0 && nb_bits_restants + stream->position <= 8) {
                nb_bits_lus = nb_bits_restants;
                // Alors on lit les bits de l'octet et on s'arrête
                // On sait que nb_bits restant <= 7
                *dest = *dest >> (8 - stream->position - nb_bits_restants);
                if (stream->position + nb_bits_restants == 8) {
                        tete_sur_octet_suiv(stream, byte_stuffing);
                }
                stream->position = (nb_bits_restants + stream->position) % 8;


        } else if (nb_octets == 0 && nb_bits_restants + stream->position > 8) {
                nb_bits_lus = (8 - stream->position);
                // On garde tout ce que l'on a lu
                // et on empiète sur l'octet suivant
                tete_sur_octet_suiv(stream, byte_stuffing);
                nb_bits_restants = nb_bits_restants - (8 - stream->position);
                // On laisse la place pour compléter 
                // avec les bits que l'on va lire
                *dest = *dest << nb_bits_restants;
                // On lit les bits restants
                uint8_t masque = 0b11111111 << (8 - nb_bits_restants);
                *dest = *dest |
                        ((stream->tete & masque)>>(8 - nb_bits_restants));
                stream->position = nb_bits_restants;
                nb_bits_lus = nb_bits_lus + nb_bits_restants;
        } else if (nb_octets == 1 && nb_bits_restants + stream->position <= 8) {
                nb_bits_lus = (8 - stream->position);
                // On garde tout ce que l'on a lu
                // et on empiète sur l'octet suivant
                tete_sur_octet_suiv(stream, byte_stuffing);
                nb_bits_restants = nb_bits_restants + stream->position;
                if (nb_bits_restants > 0) {

                        // On laisse la place pour compléter
                        // avec les bits que l'on va lire
                        *dest = *dest << nb_bits_restants;
                        // On lit les bits restants
                        uint8_t masque = 0b11111111 << (8 - nb_bits_restants);
                        *dest = *dest |
                                ((stream->tete & masque)
                                >>(8 - nb_bits_restants));
                }
                stream->position = nb_bits_restants;
                if (stream->position == 8)
                {
                    tete_sur_octet_suiv(stream, byte_stuffing);
                    stream->position = 0;
                }
                nb_bits_lus = nb_bits_lus + nb_bits_restants;
        } else if (nb_octets == 1 && nb_bits_restants + stream->position > 8) {
                nb_bits_lus = (8 - stream->position);
                // On garde ce que l'on a lu
                // et on lit l'octet suivant
                tete_sur_octet_suiv(stream, byte_stuffing);
                *dest = *dest << 8;
                *dest = *dest | stream->tete;
                nb_bits_lus = nb_bits_lus + 8;
                // Et on empiète sur celui qui suit encore
                // >0 d'après ce qui précède
                nb_bits_restants = nb_bits_restants - (8 - stream->position);
                // On garde tout ce que l'on a lu
                // et on empiète sur l'octet suivant
                tete_sur_octet_suiv(stream, byte_stuffing);
                stream->position = nb_bits_restants % 8;
                // On laisse la place pour compléter 
                // avec les bits que l'on va lire
                *dest = *dest << nb_bits_restants;
                // On lit les bits restants
                uint8_t masque = 0b11111111 << (8 - nb_bits_restants);
                *dest = *dest |
                        ((stream->tete & masque)>>(8 - nb_bits_restants));
                nb_bits_lus = nb_bits_lus + nb_bits_restants;
        } else if (nb_octets == 3 && nb_bits_restants == 0) {
                //on lit l'octet suivant
                tete_sur_octet_suiv(stream, byte_stuffing);
                *dest = (*dest << 8) | stream->tete;
                //on lit l'octet suivant
                tete_sur_octet_suiv(stream, byte_stuffing);
                *dest = (*dest << 8) | stream->tete;
                nb_bits_lus = 8 * nb_octets;
                //on se place sur l'octet suivant
                tete_sur_octet_suiv(stream, byte_stuffing);
        } else {
                printf("On ne traite pas ce cas de read_bitstream\n");
                assert(0);
        }
        return nb_bits_lus;
}

void skip_bitstream_until(struct bitstream *stream, uint8_t byte) {
        // On s'aligne sur le début de l'octet courant
        stream->position = 0;
        // On parcourt le stream jusqu'à tomber sur l'octet demandé
        while (stream->tete != byte) {
                // On lit le prochain octet 
                tete_sur_octet_suiv(stream, true);
        }
}

void free_bitstream(struct bitstream *stream) {
        fclose(stream->fichier);
        free(stream);
}