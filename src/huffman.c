#include <stdint.h>
#include <stdbool.h>
#include "bitstream.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

struct huff_table {
        /*vaut true si le noeud est une feuille et false sinon*/
        bool estfeuille;
        uint32_t val;
        struct huff_table *filsgauche;
        struct huff_table *filsdroite;
};

bool resteplace(struct huff_table *table)
/*regarde si la table est complète avec que des feuilles au dernier niveau
où si on peut rajouter des noeuds et des feuilles */ {
        if (table == NULL) {
                return true;
        } else if (table->estfeuille) {
                return false;
        } else {
                return resteplace(table->filsgauche)
                        || resteplace(table->filsdroite);
        }
}

void inserer_arbre(struct huff_table **table,
        uint32_t val,
        uint32_t taille,
        uint32_t compteurlongueur) {
        /*on veut inserer dans table la valeur val, mais on veut que la 
         * distance entre la racine et le noeud ainsi crée vaille taille 
         * compteurlongueur est un compteur qui compte notre position par 
         * rapport à la racine dans l'arbre */
        if ((*table) == NULL) {
                if (compteurlongueur == taille) {
                        /*on a trouvé où inserer
                        on crée une nouvelle feuille */
                        *table = malloc(sizeof (struct huff_table));
                        (*table)->estfeuille = true;
                        (*table)->val = val;
                        (*table)->filsgauche = NULL;
                        (*table)->filsdroite = NULL;
                } else {
                        /*on crée un fils gauche au noeud et 
                        on insère à partir de ce fils gauche
                        on crée un nouveau noeud et on inserera à l'itération
                        d'après au noeud gauche */
                        *table = malloc(sizeof (struct huff_table));
                        (*table)->estfeuille = false;
                        (*table)->filsgauche = NULL;
                        (*table)->filsdroite = NULL;
                        compteurlongueur = compteurlongueur + 1;
                        inserer_arbre(&((*table)->filsgauche),
                                val, taille, compteurlongueur);
                }
        } else if (resteplace((*table)->filsgauche)) {
                /* il reste de la place à gauche. On insère à gauche 
                 * prioritairement par construction de la table de huffman*/
                compteurlongueur = compteurlongueur + 1;
                inserer_arbre(&((*table)->filsgauche),
                        val, taille, compteurlongueur);
        } else {
                /* on doit inserer à droite */
                compteurlongueur = compteurlongueur + 1;
                inserer_arbre(&((*table)->filsdroite),
                        val, taille, compteurlongueur);
        }
};

struct huff_table *load_huffman_table(
        struct bitstream *stream, uint16_t *nb_byte_read) {
        struct huff_table *res = NULL;
        /* on crée un tableau taille qui pour chaque i allant de 0 à 15, on a
        taille[i] vaut le nombre de mot codés sur (i + 1) bits */
        uint32_t taille[16];
        /* on initialise nb_byte_read à 0 */
        *nb_byte_read = 0;
        uint8_t nbcarlu;
        uint32_t taille_effective;
        uint32_t mavaleur;
        uint32_t compteurlongueur;
        /*on commence par remplir le tableau taille */
        for (uint8_t i = 0; i < 16; i++) {
                /*on veut lire un octet, soit 8 bits
                taille_effective est ce qu'on a lu, soit la taille de tab[i]*/
                nbcarlu = read_bitstream(stream, 8, &taille_effective, 1);
                if (nbcarlu != 8) {
                        /*il y a eu une erreur, on doit sortir */
                        *nb_byte_read = -1;
                        return NULL;
                }
                /* on incrémente de 1 le nombre d'octet lu */
                *nb_byte_read = *nb_byte_read + 1;
                taille[i] = taille_effective;
        }
        /*on remplit l'arbre */
        for (uint8_t i = 0; i < 16; i++) {
                for (uint8_t j = 0; j < taille[i]; j++) {
                        /*mavaleur est la valeur représentée par le code 
                        qu'on crée automatiquement en insérant dans l'arbre*/
                        nbcarlu = read_bitstream(stream, 8, &mavaleur, 1);
                        if (nbcarlu != 8) {
                                *nb_byte_read = -1;
                                return NULL;
                        }
                        *nb_byte_read = *nb_byte_read + 1;
                        compteurlongueur = 0;
                        /*on insere mavaleur dans l'arbre 
                        le plus à gauche possible*/
                        inserer_arbre(&res, mavaleur, i + 1, compteurlongueur);
                }
        }
        return res;
}

int8_t next_huffman_value(struct huff_table *table,
        struct bitstream *stream) {
        uint32_t moncode;
        /*mon code est le code en binaire dont on veut connaitre la valeur 
        on lit bit à bit jusqu'à arriver sur une feuille*/
        if (table != NULL) {
                while (table->estfeuille == false) {
                        /* on n'est pas sur une feuille, 
                        on a donc reconnu aucun mot, il faut continuer */
                        read_bitstream(stream, 1, &moncode, 1);
                        /*on lit bit par bit */
                        if (moncode == 0) {
                                /*par construction le noeud gauche est indicé 
                                 * par 0, on va donc à gauche*/
                                table = table->filsgauche;
                        } else {
                                /*par construction le noeud droite est indicé 
                                 * par 1, on va donc à droite*/
                                table = table->filsdroite;
                        }
                }
                /* on est sur une feuille, on retourne sa valeur*/
                return table->val;
        } else {
                /*cas impossible en général*/
                printf("pas possible de lire le code");
                return -1;
        }
}

void free_huffman_table(struct huff_table *table) {
        if (table == NULL) {
                /*opération inutile, free(NULL) ne fait rien */
                free(table);
        } else {
                /*on libère les fils gauche et droit qu'on met à NULL */
                free_huffman_table(table->filsgauche);
                table->filsgauche = NULL;
                free_huffman_table(table->filsdroite);
                table->filsdroite = NULL;
                /* on libère la table en elle-même */
                free(table);
        }
}

int16_t valeur_selon_magnitude(int8_t m, uint32_t code) {
        int16_t max = pow(2, m);
        if ((code & (1 << (m - 1))) == 0) {
                return -max + 1 + code;
        } else {
                return code;
        }
}
