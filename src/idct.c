
#include <stdint.h>
#include <stdio.h>
#include <math.h>

#define CST_COS 0.196349541

float coefficient_C(uint8_t xhi) {
        if (xhi == 0) {
                // on retourne 1/sqrt(2)
                return 0.70710678118;
        } else {
                return 1;
        }
}

void idct_block(int32_t in[64], uint8_t out[64]) {
        //calcul des sommes intermédiaires dans le même format que in
        float somme_sur_nu, somme_sur_lambda;
        //calcul des colonnes et lignes
        uint32_t x, y;
        float resintermediaire;
        for (int32_t i = 0; i < 64; i++) {
                // x est la colonne
                x = i % 8;
                // y est la ligne
                y = i / 8;
                somme_sur_lambda = 0;
                // calcul de la transformée en cosinus inverse discrète
                for (int32_t lambda = 0; lambda < 8; ++lambda) {
                        somme_sur_nu = 0;
                        for (int32_t nu = 0; nu < 8; ++nu) {
                                somme_sur_nu = somme_sur_nu
                                        + (coefficient_C(nu) * cos((2 * y + 1)
 * nu * CST_COS)*(in[8 * nu + lambda]));
                        }
                        somme_sur_lambda = somme_sur_lambda
                                + (somme_sur_nu * (coefficient_C(lambda)
 * cos((2 * x + 1) * lambda * CST_COS)));
                }
                resintermediaire = 128 + ((0.25) * somme_sur_lambda);
                if (resintermediaire >= 255) {
                        resintermediaire = 255;
                }
                if (resintermediaire <= 0) {
                        resintermediaire = 0;
                }
                out[i] = (uint8_t) resintermediaire;
                // on ajoute 128 pour passer en non signée
                // toutes les variables intermédiaires étaient en int32_t;
        }
}