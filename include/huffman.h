/* Projet C - Sujet JPEG */
#ifndef __HUFFMAN_H__
#define __HUFFMAN_H__

#include <stdint.h>
#include <stdbool.h>
#include "bitstream.h"


struct huff_table;

extern struct huff_table *load_huffman_table(
		struct bitstream *stream, uint16_t *nb_byte_read);

extern int8_t next_huffman_value(struct huff_table *table, 
		struct bitstream *stream);

extern void free_huffman_table(struct huff_table *table);

extern int16_t valeur_selon_magnitude (int8_t m, uint32_t code);

extern uint32_t taille_huff_table(void);

extern uint32_t taille_huff_table_etoile(void);

extern void inserer_arbre(struct huff_table **table,
        uint32_t val,
        uint32_t taille,
        uint32_t compteurlongueur);

#endif

